start: ##@Docker start proxy
	docker network create app_network || true
	docker-compose -f infrastructure/docker-compose.yml up -d
