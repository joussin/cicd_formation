### Simple nginx-html container

To be recognize by reverse-proxy container, create a container like this:

```

version: '3.2'
services:
  nginx:
    container_name: app_nginx_8003
    build:
      context: ./../application
      dockerfile: ./infrastructure/nginx/Dockerfile
    volumes:
      - type: bind
        source: ./../application
        target: /var/www/html
    ports:
      - "8003:80"

    networks:
      - app_network

networks:
  app_network:
    external: true

```
